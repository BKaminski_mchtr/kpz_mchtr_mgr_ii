#pragma once
#include "stdafx.h"
#include <dlib/opencv.h>
#include <opencv2/highgui/highgui.hpp>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>
#include <dlib/image_processing.h>
#include <dlib/gui_widgets.h>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"
#include <iostream>
#define CHECK_PERIOD 10



using namespace dlib;
using namespace std;
using namespace cv;
class Vision2
{
public:
	Vision2();
	virtual ~Vision2();

public:
	
	int threshold_value = 0;
	int threshold_type = 3;
	int const max_value = 255;
	int const max_type = 4;
	int const max_BINARY_value = 255;

	Mat src, src_gray, dst;
	const char* window_name4 = "Threshold Demo";

	const char* trackbar_type = "Type: \n 0: Binary \n 1: Binary Inverted \n 2: Truncate \n 3: To Zero \n 4: To Zero Inverted";
	const char* trackbar_value = "Value";


	cv::Mat Ap, Ap_bin;
	int wizja();
	void Threshold_Demo(int, void*);
	double computeavg(std::vector<double> &window);
	void disp(std::vector<double> &window);
	void calibrate(std::vector<double> &pastRatios, VideoCapture &cap, frontal_face_detector &detector, shape_predictor &pose_model, image_window &win);

};

