#include "stdafx.h"
#include "Vision2.h"


Vision2::Vision2()
{
}


Vision2::~Vision2()
{
}

double Vision2::computeavg(std::vector<double> &window)
{
	double avg = 0.0;
	for (std::vector<double>::iterator i = window.begin(); i != window.end(); i++)
	{
		avg += *i;
	}

	avg /= window.size();
	return avg;
}

void Vision2::disp(std::vector<double> &window)
{
	cout << computeavg(window) << "-------";
	for (std::vector<double>::iterator i = window.begin(); i != window.end(); i++)
		cout << *i << "\t";
	cout << endl;
}

void Vision2::calibrate(std::vector<double> &pastRatios, VideoCapture &cap, frontal_face_detector &detector, shape_predictor &pose_model, image_window &win)
{
	cout << "Collecting calibration info\n";
	cv::Mat temp;
	double ratio = 0;
	for (int i = 0; i < CHECK_PERIOD * 3; i++)
	{
		cap >> temp;
		cv_image<bgr_pixel> cimg(temp);

		// Detect faces 
		std::vector<dlib::rectangle> faces = detector(cimg);

		// Find the pose of each face.
		std::vector<full_object_detection> shapes;
		for (unsigned long i = 0; i < faces.size(); ++i)
			shapes.push_back(pose_model(cimg, faces[i]));

		win.clear_overlay();
		if (shapes.empty())
		{
			win.set_image(cimg);
			//win.add_overlay(render_face_detections(shapes));
			i--;
			continue;
		}

		for (int i = 36; i <= 47; i++)
			draw_solid_circle(cimg, shapes[0].part(i), 2.0, bgr_pixel(0, 0, 255));
		win.set_image(cimg);
		//win.add_overlay(render_face_detections(shapes));

		if (i < CHECK_PERIOD)
			continue;

		ratio = (shapes[0].part(36) - shapes[0].part(39)).length_squared();
		ratio /= (shapes[0].part(37) - shapes[0].part(41)).length_squared();

		pastRatios.push_back(ratio);

		disp(pastRatios);
	}
	cout << "------------------DONE CALIBRATING------------------\n";
}
/// Function headers
//void Threshold_Demo(int, void*);



int Vision2::wizja() {

	try
	{
		cv::VideoCapture cap(0);
		if (!cap.isOpened())
		{
			cerr << "Unable to connect to camera" << endl;
			return 1;
		}

		image_window win;

		frontal_face_detector detector = get_frontal_face_detector();
		shape_predictor pose_model;
		deserialize("G:/dlib-19.4/python_examples/shape_predictor_68_face_landmarks.dat") >> pose_model;
		double ratio;
		Mat temp;
		// Grab and process frames until the main window is closed by the user.
		std::vector<double> window;
		calibrate(window, cap, detector, pose_model, win);

		double runningAvg = computeavg(window);
		while (!win.is_closed())
		{
			win.clear_overlay();
			// Grab a frame
			cap >> temp;
			cv::Mat temp2 = temp.clone();
			cv_image<bgr_pixel> cimg(temp);

			// Detect faces 
			std::vector<dlib::rectangle> faces = detector(cimg);

			// Find the pose of each face.
			std::vector<full_object_detection> shapes;
			for (unsigned long i = 0; i < faces.size(); ++i) {
				shapes.push_back(pose_model(cimg, faces[i]));
				win.clear_overlay();
			}
			win.clear_overlay();
			if (shapes.empty())
			{
				win.set_image(cimg);
				//win.add_overlay(render_face_detections(shapes));
				continue;
			}

			for (int i = 36; i <= 47; i++) {
				if (i == 36) {
					draw_solid_circle(cimg, shapes[0].part(i), 2.0, bgr_pixel(255, 255, 255));
				}
				else if (i == 39) {
					draw_solid_circle(cimg, shapes[0].part(i), 2.0, bgr_pixel(255, 255, 255));
				}
				else if (i == 42) {
					draw_solid_circle(cimg, shapes[0].part(i), 2.0, bgr_pixel(255, 255, 255));
				}
				else if (i == 45) {
					draw_solid_circle(cimg, shapes[0].part(i), 2.0, bgr_pixel(255, 255, 255));
				}
				else
					draw_solid_circle(cimg, shapes[0].part(i), 2.0, bgr_pixel(0, 0, 255));

			}
			//dlib::rectangle rect = dlib::rectangle( shapes[0].part(36),  shapes[0].part(39));
			cv::Point pp;
			cv::Point pp2;
			//cv::Point pp4 = shapes[0].part(46);
			pp.x = shapes[0].part(40).x() + 20;
			pp.y = shapes[0].part(40).y();

			pp2.x = pp.x - 60;
			pp2.y = pp.y + 55;


			cv::Rect leftEyeRect = cv::Rect(pp, pp2);


			cv::rectangle(temp, leftEyeRect, Scalar(0, 191, 255));

			//prawe oko

			cv::Point pp3;
			cv::Point pp4;
			//cv::Point pp4 = shapes[0].part(46);
			pp3.x = shapes[0].part(47).x() - 20;
			pp3.y = shapes[0].part(47).y();

			pp4.x = pp3.x + 60;
			pp4.y = pp3.y + 55;


			cv::Rect rightEyeRect = cv::Rect(pp3, pp4);


			cv::rectangle(temp, rightEyeRect, Scalar(0, 191, 255));
			namedWindow("Display window", WINDOW_AUTOSIZE);
			cv::imshow("Display window", temp);

			cv::Mat testWindow = cv::Mat(temp2, rightEyeRect).clone();
			cv::Mat testWindow2 = cv::Mat(temp2, leftEyeRect).clone();
			cv::Mat A;
			A.push_back(testWindow);
			A.push_back(testWindow2);
			//A.push_back(testWindow2);
			namedWindow("Display window2", WINDOW_AUTOSIZE);
			cv::imshow("Display window2", A);
			cv::Mat hsv = A.clone();
			cvtColor(A, hsv, CV_BGR2HSV);

			std::vector<cv::Mat> hsv_planes(3);
			Mat g = Mat::zeros(Size(hsv.cols, hsv.rows), CV_8UC1);
			//hsv_planes[0] = g;
			//hsv_planes[1] = g;
			//	hsv_planes[2] = g;
			//	hsv_planes.push_back(g);
			///	hsv_planes.push_back(g);
			//	hsv_planes.push_back(g);
			Mat HSV_chanels[3];


			namedWindow("original", 1);
			namedWindow("H", 1);
			namedWindow("S", 1);
			namedWindow("V", 1);



			cv::split(hsv, HSV_chanels);
			// not sure if this is the rigth order
			imshow("original", A);
			imshow("H", HSV_chanels[0]);
			imshow("S", HSV_chanels[1]);
			imshow("V", HSV_chanels[2]);



			cout << "tutaj zle jest" << endl;
			//Sleep(200);
			waitKey(0);
			//	hsv_planes[0] // H channel
			////		hsv_planes[1] // S channel
			//		hsv_planes[2] // V channel
			//cv::Mat HSV = hsv_planes[0].clone(); // V channel
			//	HSV.push_back(hsv_planes[1]);
			//	HSV.push_back(hsv_planes[0]);
			//HSV.push_back(hsv);
			waitKey(0);
			cout << hsv_planes.size() << endl;
			cout << hsv.rows << "__________" << hsv.cols << endl;
			//cout << hsv_planes[0].rows << "__________" << hsv_planes[0].cols << endl;
			namedWindow("Display window3", WINDOW_AUTOSIZE);
			cv::imshow("Display window3", hsv);
			namedWindow("hhh", WINDOW_AUTOSIZE);
			// cv::imshow("hhh",HSV );





			waitKey(0);
			/*
			Ap = A.clone();
			cvtColor(A, Ap, COLOR_BGR2GRAY);
			namedWindow(window_name4, WINDOW_AUTOSIZE);
			cvtColor(A,Ap, cv::COLOR_BGR2GRAY); // Convert the image to Gray

			createTrackbar(trackbar_type,
			window_name4, &threshold_type,
			max_type, Threshold_Demo); // Create Trackbar to choose type of Threshold

			createTrackbar(trackbar_value,
			window_name4, &threshold_value,
			max_value, Threshold_Demo); // Create Trackbar to choose Threshold value
			//! [trackbar]

			Threshold_Demo(0, 0); // Call the function to initialize

			/// Wait until user finishes program
			for (;;)
			{
			char c = (char)waitKey(20);
			if (c == 27)
			{
			break;
			}
			}

			waitKey(0);

			*/



			//draw_line(cimg, shapes[0].part(36), shapes[0].part(39), dlib::rgb_pixel(255, 0, 0));
			// rectangle(cimg, shapes[0].part(36), shapes[0].part(39), cv::Scalar(0, 0, 255), 1, 8, 0); nie dziala bo w d
			//polylines(cimg, ppt, npt, 2, true, randomColor(rng), rng.uniform(1, 10), lineType);
			win.set_image(cimg);
			//win.add_overlay(render_face_detections(shapes));

			ratio = (shapes[0].part(36) - shapes[0].part(39)).length_squared();
			ratio /= (shapes[0].part(37) - shapes[0].part(41)).length_squared();
			std::cout << "lllllllllllllll____________" << ratio << std::endl;
			runningAvg += (ratio - window.front()) / CHECK_PERIOD;
			window.push_back(ratio);
			window.erase(window.begin());

			//disp(window);
		}
	}
	catch (serialization_error& e)
	{
		cout << "Need landmarks.dat file first" << endl;
		cout << endl << e.what() << endl;
	}
	catch (exception& e)
	{
		cout << e.what() << endl;
	}



	return 5;
}


void Vision2::Threshold_Demo(int, void*)
{


	threshold(Ap, Ap_bin, threshold_value, max_BINARY_value, threshold_type);
	namedWindow("Display binary", WINDOW_AUTOSIZE);
	imshow("Display binary", Ap_bin);
}
